from rich.console import Console
from rich.table import Table
import inquirer
import os

# Globals
RUN_FLAG = True
to_do_list = []


def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')


def list_todos():
    if len(to_do_list) == 0:
        print("Your list is empty!")
        input("Press enter to continue...")
    else:
        i = 0
        list_len = len(to_do_list)
        console = Console()

        table = Table(show_header=True, header_style="bold white")
        table.add_column("Your to-do's:", style="dim", width=15)
        while i < list_len:
            table.add_row(
                to_do_list[i]
            )
            i = i + 1
        console.print(table)
        input("Press enter to continue...")


def add_todo():
    new_todo = input("Name of the todo: ")
    if new_todo == "**Exit**":
        print("Your todo can't be \"**Exit**\"!")
        input("Press enter to continue...")
    else:
        to_do_list.append(new_todo)
        print(f"\'{new_todo}\' was added!")
        input("Press enter to continue...")


def get_all():
    return to_do_list


def remove_todo():
    if len(to_do_list) == 0:
        print("Your list is empty!")
        input("Press enter to continue...")
    else:
        options = [
            inquirer.List('list_choice',
                          message="Which do you want to remove?",
                          choices=get_all() + ["**Exit**"],
                          ),
        ]
        answers2 = inquirer.prompt(options)
        if answers2['list_choice'] == "**Exit**":
            print("Process canceled!")
            input("Press enter to continue...")
        else:
            to_do_list.remove(answers2["list_choice"])
            print(answers2["list_choice"] + " was removed!")
            input("Press enter to continue...")


if __name__ == '__main__':
    while RUN_FLAG:
        clear_console()
        print("Welcome to my todo-manager!")
        questions = [
            inquirer.List('menu_choice',
                          message="What do you want to do?",
                          choices=['Add todo', 'List all todo\'s', 'Remove todo', 'Exit'],
                          ),
        ]

        answers = inquirer.prompt(questions)
        if answers['menu_choice'] == "Exit":
            print("Good Bye!")
            RUN_FLAG = False
        if answers['menu_choice'] == "List all todo\'s":
            list_todos()
        if answers['menu_choice'] == "Add todo":
            add_todo()
        if answers['menu_choice'] == "Remove todo":
            remove_todo()
